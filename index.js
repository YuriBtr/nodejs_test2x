import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import bodyParser from 'body-parser';
import _ from 'lodash';

const __DEV__ = true;

const app = express();
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/task2X/*', (req, res, next) => {
  let data = req.query.i;
  console.log('Requested: ' + data);

  let map = new Map([
    [0, 1],
    [1, 18],
    [2, 243],
    [3, 3240],
    [4, 43254],
    [5, 577368],
    [6, 7706988],
    [7, 102876480],
    [8, 1373243544],
    [9, 18330699168],
    [10, 244686773808],
    [11, 3266193870720],
    [12, 43598688377184],
    [13, 581975750199168],
    [14, 7768485393179328],
    [15, 103697388221736960],
    [16, 1384201395738071424],
    [17, 18476969736848122368],
    [18, 246639261965462754048]
  ]);

  console.log('Found: ' + map.get(parseInt(data, 10)));
  res.send(parseInt(map.get(parseInt(data, 10)), 10).toString(10));
  return;
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
